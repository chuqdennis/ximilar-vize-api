from pkg_resources import declare_namespace

declare_namespace("vize")

__version__ = "0.1.2"
